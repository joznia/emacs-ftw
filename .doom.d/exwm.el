;;; exwm.el -*- lexical-binding: t; -*-

(require 'exwm)
(require 'exwm-config)
(exwm-config-example)

(require 'exwm-randr)
(setq exwm-randr-workspace-monitor-plist '(1 "DisplayPort-1" 2 "DisplayPort-0" 3 "DisplayPort-2"))
(add-hook 'exwm-randr-screen-change-hook
          (lambda ()
            (start-process-shell-command
             "xrandr" nil "xrandr --output DisplayPort-1 --mode 1920x1080 --rate 120 --left-of DisplayPort-0 --output DisplayPort-0 --mode 1920x1080 --rate 240 --left-of DisplayPort-2 --output DisplayPort-2 --mode 1920x1080 --rate 240")))
(exwm-randr-enable)

(start-process-shell-command "cursor" nil "xsetroot -cursor_name left_ptr")

;; (fset 'quick_vterm
;;       (kmacro-lambda-form [?\C-x ?2 ?\C-u ?1 ?0 ?\C-x ?^ ?\C-x ?o ?\M-x ?v ?t ?e ?r ?m return] 0 "%d"))

;; (fset 'quick_eshell
;;       (kmacro-lambda-form [?\C-x ?2 ?\C-u ?1 ?0 ?\C-x ?^ ?\C-x ?o ?\M-x ?e ?s ?h ?e ?l ?l return] 0 "%d"))

;; (fset 'quick_ibuffer
;;       (kmacro-lambda-form [?\C-x ?2 ?\C-u ?1 ?0 ?\C-x ?^ ?\C-x ?o ?\M-x ?i ?b ?u ?f ?f ?e ?r return] 0 "%d"))

(fset 'kill_buffer_and_window
      (kmacro-lambda-form [?\M-x ?i ?d ?o ?- ?k ?i ?l ?l ?- ?b ?u ?f ?f ?e ?r return return ?\M-x ?d ?e ?l ?e ?t ?e ?- ?w ?i ?n ?d ?o ?w return] 0 "%d"))

;; (global-set-key (kbd (concat modk "-c")) 'quick_vterm) ; quick terminal
;; (global-set-key (kbd (concat modk "-e")) 'quick_eshell) ; quick eshell
;; (global-set-key (kbd (concat modk "-S-<return>")) 'dmenu) ; program launcher
;; (global-set-key (kbd (concat modk "-<tab>")) 'other-window) ; switch windows
;; (global-set-key (kbd "C-x b") 'quick_ibuffer)

(setq elevel 10) ;; enlarge by this much
(defun jo-enlarge ()
  (interactive) ;; needs to be interactive for some reason
  (setq count 1)
  (while (< count elevel)
    (enlarge-window-horizontally 1)
    (setq count (1+ count)))
  )

(setq slevel 10) ;; shrink by this much
(defun jo-shrink ()
  (interactive) ;; needs to be interactive for some reason
  (setq count 1)
  (while (< count slevel)
    (shrink-window-horizontally 1)
    (setq count (1+ count)))
  )

;; (defun reload-and-restart ()
;;   "Interactive function to reload the Emacs config file, then restart EXWM."
;;   (interactive)
;;   (setq qval (y-or-n-p "Restart Emacs? "))
;;   (if (equal qval "y")
;;       (load-file "~/.emacs.d/init.el")
;;     (eval qval))
;;   (if (equal qval "y")
;;       (exwm-restart)
;;     (eval qval))
;;     )

(defun reload-and-restart ()
  (interactive)
  (load-file "~/.doom.d/config.el")
  (exwm-restart)
  )

(global-set-key (kbd "M-;") 'comment-line)

(setq exwm-workspace-number 10
      exwm-randr-workspace-monitor-plist '(  0 "DisplayPort-1"
					       1 "DisplayPort-0"
					       2 "DisplayPort-2")
      exwm-input-prefix-keys '(?\M-x
       		       ?\M-:)
      ;; exwm-input-simulation-keys '(([?\s-M-F] . [?\C-f])
      ;; 				   ([?\s-M-c] . [?\C-c])
      ;; 				   ([?\s-M-x] . [?\C-x])
      ;; 				   )
      exwm-input-global-keys '(([?\s-&] . (lambda (command)
					    (interactive (list (read-shell-command "$ ")))
					    (start-process-shell-command command nil command)))
			       ;; window splits
			       ([?\s-v] . split-window-right)
			       ([?\s-x] . split-window-below)
			       ;; workspaces
			       ([?\s-w] . exwm-workspace-switch)
			       ([?\s-W] . exwm-workspace-swap)
			       ([?\s-\C-w] . exwm-workspace-move)
			       ;; launching programs
			       ([?\s-e] . dired)
			       ([s-return] . eshell)
			       ([s-S-return] . dmenu)
			       ;; killing windows
			       ([?\s-i] . ibuffer)
			       ([?\s-B] . delete-window)
			       ([?\s-C] . kill_buffer_and_window)
			       ;; change focus
			       ([?\s-j] . other-window)
       		       ([?\s-k] . windmove-up)
			       ;; resize windows
			       ([?\s-l] . jo-enlarge)
       		       ([?\s-h] . jo-shrink)
			       ;; move windows around
			       ([?\s-H] . windmove-swap-states-left)
			       ([?\s-J] . windmove-swap-states-down)
			       ([?\s-K] . windmove-swap-states-up)
			       ([?\s-L] . windmove-swap-states-right)
			       ;; restart EXWM
			       ([?\s-Q] . reload-and-restart)
			       )
      )

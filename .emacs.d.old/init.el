(tool-bar-mode 0)
(scroll-bar-mode 0)
(menu-bar-mode 0)
(global-unset-key (kbd "C-z"))
(global-linum-mode 1)
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "#1d252c" :foreground "#a0b3c5" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 140 :width normal :foundry "ADBO" :family "Ubuntu Mono")))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(doom-city-lights))
 '(custom-safe-themes
   '("c4bdbbd52c8e07112d1bfd00fee22bf0f25e727e95623ecb20c4fa098b74c1bd" "2dff5f0b44a9e6c8644b2159414af72261e38686072e063aa66ee98a2faecf0e" default))
 '(package-selected-packages
   '(smartparens highlight-parentheses rainbow-mode dmenu smooth-scrolling magit haskell-mode doom-themes yaml-mode powerline vterm ivy dracula-theme)))
(ivy-mode)
(powerline-default-theme)
(electric-pair-mode 1)
(smooth-scrolling-mode 1)

(require 'exwm)
(require 'exwm-config)
(exwm-config-example)

(require 'exwm-randr)
(setq exwm-randr-workspace-output-plist '(1 "DisplayPort-1" 2 "DisplayPort-0" 3 "DisplayPort-2"))
(add-hook 'exwm-randr-screen-change-hook
          (lambda ()
            (start-process-shell-command
             "xrandr" nil "xrandr --output DisplayPort-1 --mode 1920x1080 --rate 120 --left-of DisplayPort-0 --output DisplayPort-0 --mode 1920x1080 --rate 240 --left-of DisplayPort-2 --output DisplayPort-2 --mode 1920x1080 --rate 240")))
(exwm-randr-enable)

(start-process-shell-command "cursor" nil "xsetroot -cursor_name left_ptr")

;; (fset 'quick_vterm
;;       (kmacro-lambda-form [?\C-x ?2 ?\C-u ?1 ?0 ?\C-x ?^ ?\C-x ?o ?\M-x ?v ?t ?e ?r ?m return] 0 "%d"))

;; (fset 'quick_eshell
;;       (kmacro-lambda-form [?\C-x ?2 ?\C-u ?1 ?0 ?\C-x ?^ ?\C-x ?o ?\M-x ?e ?s ?h ?e ?l ?l return] 0 "%d"))

;; (fset 'quick_ibuffer
;;       (kmacro-lambda-form [?\C-x ?2 ?\C-u ?1 ?0 ?\C-x ?^ ?\C-x ?o ?\M-x ?i ?b ?u ?f ?f ?e ?r return] 0 "%d"))

(fset 'kill_buffer_and_window
      (kmacro-lambda-form [?\M-x ?i ?d ?o ?- ?k ?i ?l ?l ?- ?b ?u ?f ?f ?e ?r return return ?\M-x ?d ?e ?l ?e ?t ?e ?- ?w ?i ?n ?d ?o ?w return] 0 "%d"))

;; (global-set-key (kbd (concat modk "-c")) 'quick_vterm) ; quick terminal
;; (global-set-key (kbd (concat modk "-e")) 'quick_eshell) ; quick eshell
;; (global-set-key (kbd (concat modk "-S-<return>")) 'dmenu) ; program launcher
;; (global-set-key (kbd (concat modk "-<tab>")) 'other-window) ; switch windows
;; (global-set-key (kbd "C-x b") 'quick_ibuffer)

(setq elevel 10) ;; enlarge by this much
(defun jo-enlarge ()
  (interactive) ;; needs to be interactive for some reason
  (setq count 1)
  (while (< count elevel)
    (enlarge-window-horizontally 1)
    (setq count (1+ count)))
  )

(setq slevel 10) ;; shrink by this much
(defun jo-shrink ()
  (interactive) ;; needs to be interactive for some reason
  (setq count 1)
  (while (< count slevel)
    (shrink-window-horizontally 1)
    (setq count (1+ count)))
  )

;; (defun reload-and-restart ()
;;   "Interactive function to reload the Emacs config file, then restart EXWM."
;;   (interactive)
;;   (setq qval (y-or-n-p "Restart Emacs? "))
;;   (if (equal qval "y")
;;       (load-file "~/.emacs.d/init.el")
;;     (eval qval))
;;   (if (equal qval "y")
;;       (exwm-restart)
;;     (eval qval))
;;     )

(defun reload-and-restart ()
  (interactive)
  (load-file "~/.emacs.d/init.el")
  (exwm-restart)
  )

(global-set-key (kbd "M-;") 'comment-line)

(setq exwm-workspace-number 10
      exwm-randr-workspace-output-plist '(  0 "DisplayPort-1"
					      1 "DisplayPort-0"
					      2 "DisplayPort-2")
      ;; exwm-input-prefix-keys '(?\M-x
      ;; 			       ?\M-:)
      ;; exwm-input-simulation-keys '(([?\s-M-F] . [?\C-f])
      ;; 				   ([?\s-M-c] . [?\C-c])
      ;; 				   ([?\s-M-x] . [?\C-x])
      ;; 				   )
      exwm-input-global-keys '(([s-n&] . (lambda (command)
					     (interactive (list (read-shell-command "$ ")))
					     (start-process-shell-command command nil command)))
			       ;; window splits
			       ([?\s-v] . split-window-right) 
			       ([?\s-x] . split-window-below)
			       ;; workspaces
			       ([?\s-w] . exwm-workspace-switch)
			       ([?\s-W] . exwm-workspace-swap)
			       ([?\s-\C-w] . exwm-workspace-move)
			       ;; launching programs
			       ([?\s-e] . dired)
			       ([s-return] . eshell)
			       ([s-S-return] . dmenu)
			       ;; killing windows
			       ([?\s-i] . ibuffer)
			       ([?\s-B] . delete-window)
			       ([?\s-K] . kill_buffer_and_window)
			       ;; change focus
			       ([?\s-n] . other-window)
       			       ([?\s-p] . windmove-up)
			       ;; resize windows
			       ([?\s-f] . jo-enlarge)
       			       ([?\s-b] . jo-shrink)
			       ;; move windows around
			       ([?\s-P] . windmove-swap-states-up)
			       ([?\s-N] . windmove-swap-states-down)
			       ([?\s-B] . windmove-swap-states-left)
			       ([?\s-F] . windmove-swap-states-right)
			       ;; restart EXWM
			       ([?\s-Q] . reload-and-restart)
			       )
      )


(setq eshell-prompt-function (lambda nil
			       (concat
				(propertize (eshell/pwd) 'face `(:foreground "#5ec4ff"))
				(propertize " λ " 'face `(:foreground "#ebbf83")))))
(setq eshell-highlight-prompt nil)

(setq eshell-aliases-file "~/.emacs.d/aliases.txt")

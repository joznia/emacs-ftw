alias ls exa -al --color=always --group-directories-first $*
alias ff find-file $1
alias lf load-file $1
alias config /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $*
